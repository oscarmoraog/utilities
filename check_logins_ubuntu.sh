cat /var/log/auth.log | \
grep -E 'Criteo gdm-fingerprint]: gkr-pam: no password is available for user|Criteo gdm-password]: gkr-pam: unlocked login keyring' | \
sed -E 's/Criteo gdm-fingerprint]: gkr-pam: no password is available for user/Logout/g; s/Criteo gdm-password]: gkr-pam: unlocked login keyring/Login/g'
