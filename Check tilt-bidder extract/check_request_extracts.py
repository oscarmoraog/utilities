import json

file = open("test.txt")
data = file.readlines()
file.close()

def menu():
    print("""
--------------------------------------------------------------------------------------------
   
    I'm the Bidder Requests handler. Let's try to help.

    1 - Ranking of Bundle ID's
    2 - Filter by BundleID
    3 - Ranking of BundleID's with blank DeviceId 00000000-0000-0000-0000-000000000000
    4 - Ranking of IP's
    5 - Ranking of UserAgents
    6 - Ranking of DeviceId's
    
--------------------------------------------------------------------------------------------
    \n""")
    x = 0
    option = input("What's your option Today?\n")
    return option, x

def filterBundleId():
    ranking = {}
    for id, i in enumerate(data):
        line = json.loads(i)
        BundleId = line['Publisher']['BundleId']
        if(BundleId) in ranking:
            ranking[BundleId] += 1
        else:
            ranking[BundleId] = 1
        count = id + 1
    index = 0
    search = input("Write down a BundleId you'd like to filter, if you don't press Enter\n")
    print('# | Percent      |       Qty       |       Bundle ID\n')
    for key, value in ranking.items():
            if search:
                index += 1
                if key == search:
                    print(f'{index} | {"%0.2f" % ((value / count ) * 100)}%       |      {value}       |      {key}')
            else:
                print(f'{index} | {"%0.2f" % ((value / count ) * 100)}%       |      {value}       |      {key}')
                index += 1
        

def checkBundleIdRanking():
    ranking = {}
    for id, i in enumerate(data):
        line = json.loads(i)
        BundleId = line['Publisher']['BundleId']
        if(BundleId) in ranking:
            ranking[BundleId] += 1
        else:
            ranking[BundleId] = 1
        count = id + 1
    ranking = dict(sorted(ranking.items(), key=lambda x:x[1], reverse=True))
    index = 0
    top = input("How many items do you like?\n")
    print("""
    Ranking of Bundle ID's in the file
    \n# | Percent      |       Qty       |       Bundle ID\n
    """)
    for key, value in ranking.items():
        if top:
            if index <= int(top):
                print(f'{index} | {"%0.2f" % ((value / count ) * 100)}%       |      {value}       |      {key}')
                index += 1
        else:
            print(f'{index} | {"%0.2f" % ((value / count ) * 100)}%       |      {value}       |      {key}')
            index += 1


def checkBlankDeviceId():
    ranking = {}
    for id, i in enumerate(data):
        line = json.loads(i)
        BundleId = line['Publisher']['BundleId']
        DeviceId = line['Device']['DeviceId']
        if(BundleId) in ranking:
            if (DeviceId == '00000000-0000-0000-0000-000000000000'):
                ranking[BundleId] += 1
        else:
            ranking[BundleId] = 1
        count = id + 1
    ranking = dict(sorted(ranking.items(), key=lambda x:x[1], reverse=True))
    index = 0
    top = input("How many items do you like?\n")
    print("""
    Ranking of Bundle ID's with blank Device ID 00000000-0000-0000-0000-000000000000
    \n# | Percent      |       Qty       |       Bundle ID\n
    """)
    for key, value in ranking.items():
        if top:
            if index <= int(top):
                print(f'{index} | {"%0.2f" % ((value / count ) * 100)}%       |      {value}       |      {key}')
                index += 1
        else:
            print(f'{index} | {"%0.2f" % ((value / count ) * 100)}%       |      {value}       |      {key}')
            index += 1
        
def checkIPs():
    ranking = {}
    for id, i in enumerate(data):
        line = json.loads(i)
        ip = line['User']['IpAddress']
        if(ip) in ranking:
            ranking[ip] += 1
        else:
            ranking[ip] = 1
        count = id + 1
    ranking = dict(sorted(ranking.items(), key=lambda x:x[1], reverse=True))
    index = 0
    top = input("How many items do you like?\n")
    print("""
    Ranking of IP flagged in the file
    \n# | Percent      |       Qty       |       Bundle ID\n
    """)
    for key, value in ranking.items():
        if top:
            if index <= int(top):
                print(f'{index} | {"%0.2f" % ((value / count ) * 100)}%       |      {value}       |      {key}')
                index += 1
        else:
            print(f'{index} | {"%0.2f" % ((value / count ) * 100)}%       |      {value}       |      {key}')
            index += 1

def checkUAs():
    ranking = {}
    for id, i in enumerate(data):
        line = json.loads(i)
        ua = line['User']['UserAgent']
        if(ua) in ranking:
            ranking[ua] += 1
        else:
            ranking[ua] = 1
        count = id + 1
    ranking = dict(sorted(ranking.items(), key=lambda x:x[1], reverse=True))
    index = 0
    top = input("How many items do you like?\n")
    print("""
    Ranking of User Agents flagged in the file
    \n# | Percent      |       Qty       |       Bundle ID\n
    """)
    for key, value in ranking.items():
        if top:
            if index <= int(top):
                print(f'{index} | {"%0.2f" % ((value / count ) * 100)}%       |      {value}       |      {key}')
                index += 1
        else:
            print(f'{index} | {"%0.2f" % ((value / count ) * 100)}%       |      {value}       |      {key}')
            index += 1

def checkDeviceIDs():
    ranking = {}
    for id, i in enumerate(data):
        line = json.loads(i)
        ua = line['Device']['DeviceId']
        if(ua) in ranking:
            ranking[ua] += 1
        else:
            ranking[ua] = 1
        count = id + 1
    ranking = dict(sorted(ranking.items(), key=lambda x:x[1], reverse=True))
    index = 0
    top = input("How many items do you like?\n")
    print("""
    Ranking of DeviceID's flagged in the file
    \n# | Percent      |       Qty       |       Bundle ID\n
    """)
    for key, value in ranking.items():
        if top:
            if index <= int(top):
                print(f'{index} | {"%0.2f" % ((value / count ) * 100)}%       |      {value}       |      {key}')
                index += 1
        else:
            print(f'{index} | {"%0.2f" % ((value / count ) * 100)}%       |      {value}       |      {key}')
            index += 1


option, x = menu()
while(x == 0):
    match option:
            case '1':
                checkBundleIdRanking()
                option, x = menu()
            case '2':
                filterBundleId()
                option, x = menu()    
            case '3':
                checkBlankDeviceId()
                option, x = menu()     
            case '4':
                checkIPs()
                option, x = menu()
            case '5':
                checkUAs()
                option, x = menu()      
            case '6':
                checkDeviceIDs()
                option, x = menu()                     
            case '10':
                x = 1
            # If an exact match is not confirmed, this last case will be used if provided
            case _:
                print("Sorry, something's wrong with me\n")


