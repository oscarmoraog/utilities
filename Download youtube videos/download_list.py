import yt_dlp

URLS = [
    'https://www.youtube.com/watch?v=jsAn9AKWK40',
    'https://www.youtube.com/watch?v=hUOwvMXP1Eg',
    'https://www.youtube.com/watch?v=s7FziyRGT9s',
    'https://www.youtube.com/watch?v=ShnrP9moEww',
    'https://www.youtube.com/watch?v=n9ykiGpGgbU&t=815s',
    'https://www.youtube.com/watch?v=ko7VtwLR3lg&t=161s',
    'https://www.youtube.com/watch?v=9vAPWAPz8AQ',
    'https://www.youtube.com/watch?v=xy4rmlnRo6s',
    'https://www.youtube.com/watch?v=5P8GcCpmGYQ',
    'https://www.youtube.com/watch?v=3deru6nPhlk' ,
    'https://www.youtube.com/watch?v=QuCGzFbrdJM'      
    ]

def format_selector(ctx):
    """ Select the best video and the best audio that won't result in an mkv.
    NOTE: This is just an example and does not handle all cases """

    # formats are already sorted worst to best
    formats = ctx.get('formats')[::-1]

    best_video = next(f for f in formats
                    # acodec='none' means there is no audio
                    if f['vcodec'] != 'none' and f['acodec'] == 'none'
                    # desired format
                    and f['ext'] in ['mp4']
                    )

    # find compatible audio extension
    audio_ext = {'mp4': 'm4a', 'webm': 'webm'}[best_video['ext']]
    # vcodec='none' means there is no video
    best_audio = next(f for f in formats if (
        f['acodec'] != 'none' and f['vcodec'] == 'none' and f['ext'] == audio_ext))

    # These are the minimum required fields for a merged format
    yield {
        'format_id': f'{best_video["format_id"]}+{best_audio["format_id"]}',
        'ext': best_video['ext'],
        'requested_formats': [best_video, best_audio],
        # Must be + separated list of protocols
        'protocol': f'{best_video["protocol"]}+{best_audio["protocol"]}'
    }

ydl_opts = {
    'format': format_selector,
}

with yt_dlp.YoutubeDL(ydl_opts) as ydl:
    ydl.download(URLS)        