# Documentation
https://github.com/yt-dlp/yt-dlp

## Dependencies
https://www.ffmpeg.org/download.html#build-linux

sudo apt update
sudo apt install ffmpeg

## yt_dlp options
https://github.com/ytdl-org/youtube-dl/blob/master/youtube_dl/YoutubeDL.py#L128-L278

## Example:
python yt-dlp https://www.youtube.com/watch?v=n9ykiGpGgbU
