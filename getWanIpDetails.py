import pycurl
from io import BytesIO
#from urllib.parse import urlencode
import json

def curl(URL):
    buffer = BytesIO() 
    c = pycurl.Curl() 
    c.setopt(c.URL, URL)
    c.setopt(c.WRITEDATA, buffer)
    c.perform() 
    c.close()
    get_body = buffer.getvalue()
    return get_body
    

body = curl('https://ipapi.co/json/')
if(body):
    body = json.loads(body)
    print(json.dumps(body, indent=4))
else:
    print('No Body')