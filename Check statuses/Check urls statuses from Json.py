import requests
import json

object = {
    "id": "13726538542401199864000",
    "cur": "USD",
    "seatbid": [
        {
            "seat": "ES",
            "bid": [
                {
                    "id": "63f62693abbeef33856fddbf7c640000",
                    "impid": "1",
                    "price": 0.02,
                    "nurl": "https://rtb.fr.eu.criteo.com/triplelift/auction/notify?profile=75&imp=3ImmRZEV8ReKkJtvTQf0MCuz9hcw7Lt0qn1zM3rhuUCp-_xPHdxBv2-6WTl4WQw8AyShtLB3sbpM60ZeQwTJ19W4JYmnw3e9jG2Z68bg_QM-bC5vBES-1tNU3_drKSV9XHt4NnY6TO2bx6oIJN3e83vWPT2parT9z9hdyQ8ri5GdN2Tyr5cE2uibT0CGmeqMNptcQum4rw_YSq0xUWwbO7xLvr5fGNE4wMBjc4py9uMwKxWr7PY44MmGkS2Z8GHsSYzEXS6mC2oIYD61GbpZ0bgvDqTnnxqyVif0Jeg3-dukjikDK1oM3Pi3aZVnlEhsNhxcKHUh1zkjvOtvK8DM4fP0Hqv0Q25CKfmm9MKvGfIiq4qz_26lFf1YC80lTbP6XvJk2ou7X8acQIxxcyEyfw8LDKbaE0jZLL8l7b4MDahCdaup-JoKR3NnO-e_1Q16BxSjAcp3giF16dj4C2HdgZWcC9jKCYR1lBXtJ5TGFTkaVZyqX3Ou89l_Yg3VZ4gI9vylZIeTEN53oCMZibM8rTMvl5x61MwUdKl5Tx5BvvIVN6W2cw8fdAJuRLI27WgwfgtRPRj59pcpWzGQ21_FEkql0EhXXe_XoRIkeXXnbIhYmaqwp1XGkT6buBr6w_gKtBFdIw6IOOY5GH-IK_e_0AfpTVlLWnwJBgArF6rTR0pA7ZA-GGhTP8xOqTps5AKRbJnmW00tWFt0f8TdmUjRLumLoZtWrneGpOv9xouT_fD9Swm1V9yjNWl1geOn13I6vbqWBG5XokEviv_RCderncH-WCasZpDMz6ahI7McKpg&price=${AUCTION_PRICE}",
                    "lurl": "https://rtb.fr.eu.criteo.com/triplelift/auction/notify?profile=75&imp=JgznpcM1eI9pdWzeO67-2KBadY70BznrP0Wv3HBn5P2xwnnonrTFEoayhBI__bWg_C4ey1PeyUBjuMubGLC6wZWEa1ESSaLSeTlBGp932X4LTiD_lazvSzdM-NN9lt2JMYp9j5GvwTnTZ35XBeo6wLkjncLp3t8uJpGyxBPmfkNWf92Xz4Z-x_ONt4UWnuj2I690SBpVuEMCeqSmA0fV2MS7A4riY-V8C2a3nAV83qMT2Z7g8kbX1gJgfyK0k_7J6rVzCw9BxoinR4Yp7USskSdgYYBnnHQIfQ9coa1T7huvbgxQ2vs9wycWnvzZzCwk8wHrefUeAWZavbD8svOqlWkD3vsguVwmpplfgN32tQKQcH7_0FawfEJCCs6grUBLMMqvhdw8fc5oGQQJe2HlLZiKpDnzk-mAnQ2iVrEi-Rp29PGnv6da4uSTJxpphAaUzQrdtYzYKSURdlz2M3tb5cq4_LqEvD_MQ42ugR9EUu4va7ciO9BdnydSXr1gA_ZptG1NqLwQoq32qu-jThvEav1TX-2oj-1z6LyROTm6vJgun8bm1bjkxYKk8lex-CoU_XDAYAbnLetyYUR3CG4uNoIH0iXeZoofkYBzxV-K2Jw_Y4zhaU5O5qXYtwOpm7mCeng6Ucv6nUnapwKi8SrtB-V_lUP3blT_m60hKiw-lakvWPtwk_BRJ9lXrNOXflW8EIOin4k_vIRB415JGXkyvgEbRiqkW9onAvKmqW_F8WnaiNbS5jslFPl-YEu6wqeaRguXhp_5HS-jvZAK7wgQbiKbZCgmF4QHTEYkFaZml9U&st=${AUCTION_LOSS}",
                    "crid": "10383660",
                    "ext": {
                        "prodid": "52244_dc0448",
                        "heading": "Calcetines Cortos Soft De Casa Natural",
                        "caption": "Calzedonia Online Store",
                        "imptrackers": [],
                        "imgUrl": "https://pix.eu.criteo.net/img/img?c=3&cq=256&h=400&m=0&partner=57179&q=80&r=0&u=https%3A%2F%2Fwww.calzedonia.com%2Fdw%2Fimage%2Fv2%2FBJHW_PRD%2Fon%2Fdemandware.static%2F-%2FSites-CAL_EC_COM%2Fdefault%2Fimages%2FDC04489515-FI.jpg%3Fsfrm%3Djpeg%26sw%3D400%26sh%3D600&ups=1&v=3&w=400&s=GDXDOoaItdE91WSxG60cj7te",
                        "clickUrl": "https://cat.fr.eu.criteo.com/delivery/ckn.php?cppv=3&cpp=VjqLVzulvJmihkNce-ccaP4RZtATmFu4vEvJtPqj0iRvdiMjNBVsZgrss3p9YXHRe5f1clYzIjCYm0l2-BeXTzfdmGBxsgO-OEngsUkZxBLmGFNeuN_LkrsOHQC20UoeH_rBwvPm-E2cugpaQa8sVsmgH7jLD40YiWLHCcoCoghYy6msCZY_ZNUyTWllNkGrwfklRn6o3VX1joQtlUb7A5k2AUp4Pa_bwIXqTyJMIcaxHRGFbHaJuNSI67olb5k07xKNjRlMzLZUROrZKdjOBqjcPEKmBR-LmTulsFMreYKTARY0PQ0X5OfPwUSbVbgruLohsdRpAYLC8gH1V3ZEMGl1mhps5f1eF6wruExcWHehZVX-_1g2DcadhegAZ6_SxFs3do8dgyaBBVl-mSAXK53XHmD5tsZ1w4sKjjAr9bN6wUT-0TgHktmuL3ijBq3kkltzPvmnMt8eQHB3CfQgz98Ef-GKJN4fAh7aKFsV7ztoZCiS&maxdest=https%3A%2F%2Fad.doubleclick.net%2Fddm%2Ftrackclk%2FN1757938.1922089CRITEO.ES%2FB28038612.339134518%3Bdc_trk_aid%3D530777867%3Bdc_trk_cid%3D173142412%3Bdc_lat%3D%3Bdc_rdid%3D%3Btag_for_child_directed_treatment%3D%3Btfua%3D%3Bgdpr%3D1%3Bgdpr_consent%3DCPnlY4APnlY4AAKAuAENC4CsAP_AAH_AABBYJQtd_H__bW9r-f5_aft0eY1P9_r77uQzDhfNk-4F3L_W_LwX52E7NF36tq4KmR4ku1LBIUNlHNHUDVmwaokVryHsak2cpTNKJ6BEknMZO2dYGF5vmxtj-QKY5v5_d3bx2D-t_9v-39z3z81Xn3d5_-_02PCdU5_9Dfn9fR_b89KP9_78v4v8_9_rk3_e__3_79_7_H8-CTYBJhq3EAXZljgzaBhFAiBGFYSEUCgAgoBhaICABwcFOysAn1hEgAQCgCMCIEOAKMCAQAACQBIRABIEWCAAAEQCAAEACARCABgYBBYAWBgEAAIBoGKIUAAgSAGRARFKYEBUCQQEtlQglBdIaYQBVlgBQCI2CgARBICKwABAWDgGCJASsWCBJiDaIARgBQCiVCtRSemgAAAA.f_gAAAAAAAAA%3Bltd%3D%3Fhttps%3A%2F%2Fwww.calzedonia.com%2Fes%2Fproduct%2Fcalcetines_cortos_soft_de_casa-DC0448.html%3Fdwvar_DC0448_Z_COL_CALZ%3D9515%26utm_source%3Dcriteo%26utm_medium%3Dretargeting%26utm_campaign%3D%5BES_PERF%5D_lowrfunnel_clz",
                        "advName": "Calzedonia",
                        "logoUrl": "https://pix.eu.criteo.net/img/img?h=1200&m=0&partner=57179&q=80&r=0&u=http%3A%2F%2Fstatic.fr.eu.criteo.net%2Fdesign%2Fdt%2F57179%2F191126%2F88f4ca4f0bde4d379ec5404d6a959546_logo_n_horizontal.png&v=3&w=1200&s=lsEl8D9yrhMmWxM1va-i3Wtc",
                        "useAdchoices": "true",
                        "adchoicesOverrideUrl": "https://privacy.eu.criteo.com/adchoices?cppv=3&cpp=rsj8dKedmQjfTNjx0uZR3Ya54P5hjnMTMOXSKO2r71HBq3Dat3UW5dPIHMvW_9etkYxR_ycERFC-DWzlNw6GpdSviHlDT1uFA2r3vAiH1_CsKj3APg0SHzg4Ib81IMV8rwhhQ6lZDrSzoqzOmDxpn8N13eqj2YnPe5EP0sJxU2lDIV97"
                    }
                }
            ]
        }
    ]
}


def rec_keys(dictio):
    keys = []
    for (key, value) in dictio.items():
        if isinstance(value, dict):
            keys.extend(rec_keys(value))
        elif isinstance(value, list):
            for i in value:
                if isinstance(i, dict):
                    keys.extend(rec_keys(i))
        else:
            if isinstance(value, str):
                if value.startswith('http'):
                    resp = requests.get(url=value)
                    print(str(key) + ": " +
                           # ": \n Value: " + str(value) + "\n" +
                            str(resp.status_code))
    return keys

print(str(rec_keys(object)) + "\n")
